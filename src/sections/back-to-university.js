import React from "react";
import { Container, TitleAndAnnotation, ImageWithBigText } from "../components";
import styled from "styled-components";
import { size } from "../theme/theme";

const Space = styled.div`
  margin: 100px 0;
  @media ${size.m} {
    margin: 160px 0;
  }
`;

const backToUniversity = ({ mainContent, imageWithText }) => {
  return (
    <>
      <Container>
        <TitleAndAnnotation
          withLeftLine={mainContent.showLine}
          title={mainContent.title}
          annotation={mainContent.annotation}
        />

        <Space> </Space>

        <ImageWithBigText
          gatsbyImage={imageWithText.image.asset.fluid}
          text={imageWithText.text}
        ></ImageWithBigText>
      </Container>
    </>
  );
};

export default backToUniversity;
